%% this file plots the channels. do not change!
clear all global
close all

%% init obob_ownft
addpath('obob_ownft');

obob_init_ft;

%% set vars...
layout = 'biosemi64.lay';

%% plot layout...
cfg = [];
cfg.layout = layout;

lay = ft_prepare_layout(cfg);

ft_plot_lay(lay);