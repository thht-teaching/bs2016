% This script extracts the event related data from the matfiles and creates
% a csv file that you can then use with SPSS. You will find a section with
% variables that you can set accordingly to choose electrodes and
% timeframes.

%% this section clears all variables and sets some defaults. do not change anything here
clear all global
close all

layout = 'biosemi64.lay';

%% This is the section in which you specify what kind of data you want to export.
%  You also need to specify a filename where to put the data in.

% specify the channels here. the default (channels = 'all') gives you all
% channels. no averaging over channels is done here.
% if you only want some channels, you need to specify them like this:
% channels = {'O1', 'Oz', 'O2'};
% if you want to see where the channels are and how they are called, just
% enter show_channels in the matlab window

channels = 'all';

% specify the time-frame here. it looks like this: timeframe = [-0.5 -0.1];
% this would give you the data between -500ms and -100ms.

timeframe = [-0.5 -0.1];

% specify the frequencies here. it looks like this: frequencies = [5 20];
% this would give you the frequencies between 5 and 20 Hz.

frequencies = [5 20];

% specify if you what data you want. possible values are:
% 'performance': averaged data for correct and incorrect trials
% 'awareness': unaveraged trials grouped according to awareness ratings
% 'strength': unaveraged trials grouped according to stimulus strength/contrast
%
% this MUST be specified in curly braces. e.g.;
% data_selection = {'performance', 'awareness'};
%
% ALSO if you only want one of them!!!!!
% data_selection = {'performance'};

data_selection = {'performance', 'awareness', 'strength'};

% specify here if you want complex values. normally you do not, so leave it
% at: want_complex = false

want_complex = false;

% specify here if you want to retain single trials for the performance
% measurements. please note that if you ask for complex values you will
% always get single trials!

want_single_trials = true;

% specify the folder you want your data written to here. please be aware that
% no checks are done whether the folder or files therein already exist!

folder = 'tf';

%% leave it alone from here.

%% init obob_ownft...
addpath('obob_ownft/');

obob_init_ft;

addpath('functions');

%% load all the data...
datapath = 'data/wavelet';
allfiles = dir(fullfile(datapath, '*.mat'));
allfiles = {allfiles.name};

mkdir(folder);

for file = allfiles
  [~, subject_id, ~] = fileparts(file{1});
  
  load(fullfile(datapath, file{1}));
  
  % select data....
  cfg = [];
  cfg.channel = channels;
  cfg.latency = timeframe;
  cfg.frequency = frequencies;
  
  data = ft_selectdata(cfg, data_fourier);
  clear data_fourier;
  
  if ~want_complex
    cfg = [];
    cfg.keeptrials = 'yes';
    
    data = ft_freqdescriptives(cfg, data);
  end %if
  
  % get performance
  if match_str(data_selection, 'performance')
    tmp = [];
    
    cfg = [];
    if want_complex || want_single_trials
      cfg.keeptrials = 'yes';
    end %if
    
    cfg.trials = data.trialinfo(:, 4) == 0;
    
    if want_complex
      tmp.incorrect = ft_selectdata(cfg, data);
    else
      tmp.incorrect = ft_freqdescriptives(cfg, data);
    end %if
    
    cfg.trials = data.trialinfo(:, 4) == 1;
    
    if want_complex
      tmp.correct = ft_selectdata(cfg, data);
    else
      tmp.correct = ft_freqdescriptives(cfg, data);
    end %if
    
    data_to_csv(folder, subject_id, 'performance', 'incorrect', tmp.incorrect);
    data_to_csv(folder, subject_id, 'performance', 'correct', tmp.correct);
  end %if
  
  if match_str(data_selection, 'awareness')
    all_awareness = unique(data.trialinfo(:, 5));
    
    for i = all_awareness'
      fieldname = sprintf('awareness_%d', i);
      
      cfg = [];
      cfg.keeptrials = 'yes';
      cfg.trials = data.trialinfo(:, 5) == i;
      
      if want_complex
        tmp = ft_selectdata(cfg, data);
      else
        tmp = ft_freqdescriptives(cfg, data);
      end %if
      
      data_to_csv(folder, subject_id, 'awareness', fieldname, tmp);
    end %for
  end %if
  
  if match_str(data_selection, 'strength')
    all_strength = unique(data.trialinfo(:, 6));
    
    for i = all_strength'
      fieldname = sprintf('strength_%d', i);
      
      cfg = [];
      cfg.keeptrials = 'yes';
      cfg.trials = data.trialinfo(:, 5) == i;
      
      if want_complex
        tmp = ft_selectdata(cfg, data);
      else
        tmp = ft_freqdescriptives(cfg, data);
      end %if
      
      data_to_csv(folder, subject_id, 'strength', fieldname, tmp);
    end %for
  end %if
end %for
