function data_to_csv(folder, subject, field, subfield,  data )
%DATA_TO_TABLE Summary of this function goes here
%   Detailed explanation goes here

if ~isfield(data, 'trial') && isfield(data, 'avg')
  
  t = convertData(data.label, data.time, data.avg);
  
  fname = sprintf('%s_%s_%s.csv', subject, field, subfield);
  
  writetable(t, fullfile(folder, fname));
elseif isfield(data, 'trial')
  for i = 1:size(data.trial, 1)
    t = convertData(data.label, data.time, squeeze(data.trial(i, :, :)));
    
    fname = sprintf('%s_%s_%s_%03d.csv', subject, field, subfield, i);
    
    writetable(t, fullfile(folder, fname));
    
  end %for
  
elseif isfield(data, 'powspctrm') || isfield(data, 'fourierspctrm')
  if isfield(data, 'powspctrm')
    spctrm = data.powspctrm;
  else
    spctrm = data.fourierspctrm;
  end %if
  
  if ndims(spctrm) == 3
    % no single trials....
    for i = 1:size(spctrm, 2)
      t = convertData(data.label, data.time, squeeze(spctrm(:, i, :)));
      
      fname = sprintf('%s_%s_%s_freq_%02d.csv', subject, field, subfield, round(data.freq(i)));
      writetable(t, fullfile(folder, fname));
    end %for
  elseif ndims(spctrm) == 4
    for i = 1:size(spctrm, 3)
      for j = 1:size(spctrm, 1)
        t = convertData(data.label, data.time, squeeze(spctrm(j, :, i, :)));
        
        fname = sprintf('%s_%s_%s_freq_%02d_trial_%03d.csv', subject, field, subfield, round(data.freq(i)), j);
        writetable(t, fullfile(folder, fname));
      end %for
    end %for
  end %if
  
end %if


end

function t = convertData(labels, time, dat)
t = table(labels, dat);
t_time = table({'time'}, time);
t.Properties.VariableNames = {'Channel', 'Data'};
t_time.Properties.VariableNames = {'Channel', 'Data'};

t = [t; t_time];

end %function

