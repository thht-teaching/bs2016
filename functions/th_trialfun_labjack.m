function [ trl, evt ] = th_trialfun_labjack( cfg )
%TH_TRIALFUN_LABJACK Summary of this function goes here
%   Detailed explanation goes here
%
% only set cfg.trialdef.prestim and cfg.trialdef.poststim....
% output trialinfo: #trial, answer, orientation, correct/incorrect,
% awareness, stimstrength

onsettriggers = [11 12 13 14];

evt = ft_read_event(cfg.dataset);
hdr = ft_read_header(cfg.dataset);

offsetsample = -cfg.trialdef.prestim * hdr.Fs;
postsample = cfg.trialdef.poststim * hdr.Fs;

% sort out new segment marker....
evt = ft_filter_event(evt, 'type', 'Stimulus');

% sort out bad ones....
x = [evt.sample];
xx = [+inf diff(x)];

evt = evt(xx > 20);

for i = 1:length(evt)
  evt(i).value = str2double(evt(i).value(2:end));
end %for

allvalues = [evt.value];
idx = find(ismember(allvalues, onsettriggers));

trl = zeros(length(idx), 9);

for i = 1:length(idx)
  trl(i, 1) = evt(idx(i)).sample + offsetsample;
  trl(i, 2) = evt(idx(i)).sample + postsample;
  trl(i, 3) = offsetsample;
  trl(i, 4) = i;
  trl(i, 5) = evt(idx(i)).value - 10;
  trl(i, 6) = evt(idx(i)+2).value - 30;
  trl(i, 7) = trl(i, 5) == trl(i, 6);
  trl(i, 8) = evt(idx(i)+3).value - 40;
  trl(i, 9) = evt(idx(i)-1).value - 100;
end %for

end

