%% This is a short and quick tutorial, how to do sensory analysis of EEG data in FieldTrip
% I will explain everything step by step.

%% first, we want to tell Matlab to delete everything, so we start fresh
clear all global

%% also open windows (plots) would be bad, so we close those...
close all

%% Matlab itself does not really know how to handle EEG data. This is why
% we use a toolbox called FieldTrip to do it. FieldTrip is very powerful
% and widely used in MEG/EEG research.
% Because our group has also written some additional functions, we decided
% to put everything together into a distribution called "obob_ownft". You
% can download it for free from here: https://gitlab.com/obob/obob_ownft It
% already includes FieldTrip, so no need to download it separately.
% In order to use it, we need to initialize it. If you do not understand
% what is happening here, do not worry. You can copy and paste the
% following two lines to all your analysis scripts and it will work as long
% as the obob_ownft folder is a child of the current folder....

addpath('obob_ownft');

obob_init_ft;

%% we also need some functions that are specific to our analysis. These
% life in the folder called 'functions'. In order to use them, we need to
% tell Matlab about them:

addpath('functions');

%% and we set some variables that are important later:
layout = 'biosemi64.lay';

%% ok, the next step is reading in the data.
% if you take a look at the folder: 'data/raw' you see 3 files. This is
% what the amplifier gave us. So, lets take a look and read it all...

cfg = [];
cfg.dataset = fullfile('data', 'raw', 'vp02.eeg');
cfg.trialdef.triallength = Inf;

cfg = ft_definetrial(cfg);

data = ft_preprocessing(cfg);

%% so, now we have all our data. lets take a look....
cfg = [];
cfg.viewmode = 'vertical';

ft_databrowser(cfg, data);

%% However, you remeber, that we do not want to
% have all the data.... We only want the trials. I.e., everytime a stimulus
% came. And we also need information about the trial like: Did the
% participant answer correctly? What was the awareness? What was the
% strength of the stimulus and so on....
%
% So, the next step is not actually reading the data but looking at the
% triggers and defining, what segments of data we actually need.

cfg = [];
cfg.trialfun = 'th_trialfun_labjack'; % This is our custom function to analyze all the triggers
cfg.dataset = fullfile('data', 'raw', 'vp02.eeg');
cfg.trialdef.prestim = 3; % we want to have three seconds before stim onset and...
cfg.trialdef.poststim = 3; % three seconds afterwards. This look like a lot, but we need it to avoid some artifacts from filtering...

cfg = ft_definetrial(cfg);
trl = cfg.trl;

%% now we actually read the data...
% but while we do this, we can tell FieldTrip to do two more things for us:
%
% 1. Have you seen the slow fluctuations when you looked at the data? We
% will use a highpass filter to get rid of those
%
% 2. You might remember that the A1 and A2 channels were not really
% reliable. We will not use them in our current analysis, so we can tell
% FieldTrip to not include them....

cfg.channel = {'all', '-A1', '-A2'};
cfg.hpfilter = 'yes';
cfg.hpfreq = 1;

data = ft_preprocessing(cfg);

%% the data is sampled with 1000Hz. This is great for recording but slows down
% the analysis. So, we will lower that to 200Hz

cfg = [];
cfg.resamplefs = 200;
cfg.demean = 'no';

data = ft_resampledata(cfg, data);

%% remember that we took huge segments (6 seconds!) of data because of some
% filtering artifact? This was the highpass filter in the previous step.
% So, lets cut throw away what we do not need....

cfg = [];
cfg.toilim = [-1 1];

data = ft_redefinetrial(cfg, data);

%% now, lets take a look again!
cfg = [];
cfg.viewmode = 'vertical';

ft_databrowser(cfg, data);

%% remember that we told our participants, not to blink?
% well, this one was really a good one, but take a look at trial #13. you
% see a huge blink there! so, we need to delete the trials with blinks.
% Luckily we have our EOG channels, so we can use them to define a
% threshold...

cfg = [];
cfg.trl = trl;
cfg.artfctdef.zvalue.channel = {'hEOG', 'vEOG'};
cfg.artfctdef.zvalue.bpfilter = 'yes';
cfg.artfctdef.zvalue.bpfreq = [1 15];
cfg.artfctdef.zvalue.cutoff = 4;
cfg.artfctdef.zvalue.interactive = 'no';
cfg.artfctdef.zvalue.rectify = 'yes';

cfg_art = ft_artifact_zvalue(cfg, data);

cfg_art.artfctdef.reject = 'complete';

data = ft_rejectartifact(cfg_art, data);

%% now, we do not need the EOG channels anymore....
cfg = [];
cfg.channel = {'all', '-hEOG', '-vEOG'};

data = ft_selectdata(cfg, data);

%% as you can see, more than half of the trials were rejected! this is actually
% not as bad as it sounds and the reason why we always use so many trials.
% but, lets take a look again....

cfg = [];
cfg.viewmode = 'vertical';

ft_databrowser(cfg, data);

%% ok, this looks much better... But the data is still not clean. For example,
% the dead channel will really mess things up. So, lets try to get a nice
% overview.
% As you will see, you can easily make out the dead channel and remove it.
% The rest of the data looks pretty clean. So we do not have to remove more
% trials

cfg = [];
cfg.latency = [-1 .5];
cfg.preproc.lpfilter = 'yes';
cfg.preproc.lpfreq = 35; % but we do not care about 50Hz.

data = ft_rejectvisual(cfg, data);

%% The last thing we need to do is to subtract the average over all remaining channels at every
% sample. This is called "average reference". The beauty of this method is
% that you emphasize the difference between channels. In other words, your
% results get clearer.

cfg = [];
cfg.reref = 'yes';
cfg.refchannel = 'all';

data = ft_preprocessing(cfg, data);

%% Now we have clean data. What can we do with it?
% The next logical step is to look at the so-called evoked potential. This
% is basically the reaction of the brain to an incoming stimulus. It is
% calculated quite easily: Just average over trials.
% However, they get a lot clearer if we filter out high frequencies, so we
% do this as well...

cfg = [];
cfg.preproc.lpfilter = 'yes';
cfg.preproc.lpfreq = 30;

erps_all = ft_timelockanalysis(cfg, data);

%% so, lets take a look...
cfg = [];
cfg.layout = layout;

ft_multiplotER(cfg, erps_all);

%% however...this does not really tell us something.... what we want to
% do is see whether there is a difference between conditions, like correct
% and incorrect responses.
% to do that, we want to do the ft_timelockanalysis only for correct trials
% and then only for incorrect trials. but how do we know which trial is
% which?
% If you look at the data structure, you will find a field called
% 'trialinfo'. This holds "metadata" for every trial. And in fact, the
% fourth column is 0 for incorrect trials and 1 for correct ones!

cfg = [];
cfg.preproc.lpfilter = 'yes';
cfg.preproc.lpfreq = 30;

cfg.trials = data.trialinfo(:, 4) == 0;

erps_incorrect = ft_timelockanalysis(cfg, data);

cfg.trials = data.trialinfo(:, 4) == 1;

erps_correct = ft_timelockanalysis(cfg, data);

%% so, lets take a look....
cfg = [];
cfg.layout = layout;

ft_multiplotER(cfg, erps_correct, erps_incorrect);

%% and this is how to do time-frequency analysis...
cfg = [];
cfg.method = 'wavelet';
cfg.output = 'pow';
cfg.foi = 2:1:20;
cfg.toi = -.7:.05:.5;

pow = ft_freqanalysis(cfg, data);

%% the correct/incorrect effect is too weak to see in just one subject...
% but a very nice effect is the alpha desynchronization at stimulus onset.
% so, we normalize by the prestimulus activity....

cfg = [];
cfg.baselinetype = 'relchange';
cfg.baseline = [-.7 -.2];

pow_bl = ft_freqbaseline(cfg, pow);

%% lets take a look...
cfg = [];
cfg.zlim = [-.5 .5];
cfg.layout = layout;

ft_multiplotTFR(cfg, pow_bl);