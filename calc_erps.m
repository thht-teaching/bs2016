% This script extracts the event related data from the matfiles and creates
% a csv file that you can then use with SPSS. You will find a section with
% variables that you can set accordingly to choose electrodes and
% timeframes.

%% this section clears all variables and sets some defaults. do not change anything here
clear all global
close all

layout = 'biosemi64.lay';

%% This is the section in which you specify what kind of data you want to export.
%  You also need to specify a filename where to put the data in.

% specify the channels here. the default (channels = 'all') gives you all
% channels. no averaging over channels is done here.
% if you only want some channels, you need to specify them like this:
% channels = {'O1', 'Oz', 'O2'};
% if you want to see where the channels are and how they are called, just
% enter show_channels in the matlab window

channels = 'all';

% specify the time-frame here. it looks like this: timeframe = [0.1 0.3];
% this would give you the data between 100ms and 300ms. erp data are
% downsampled to 200Hz, so you get one sample each 5ms.
% no averaging over samples is done here.

timeframe = [0.1 0.3];

% specify if you what data you want. possible values are:
% 'performance': averaged data for correct and incorrect trials
% 'awareness': unaveraged trials grouped according to awareness ratings
% 'strength': unaveraged trials grouped according to stimulus strength/contrast
%
% this MUST be specified in curly braces. e.g.;
% data_selection = {'performance', 'awareness'};
%
% ALSO if you only want one of them!!!!!
% data_selection = {'performance'};

data_selection = {'performance', 'awareness', 'strength'};

% specify the folder you want your data written to here. please be aware that
% no checks are done whether the folder or files therein already exist!

folder = 'erps';

%% leave it alone from here.

%% init obob_ownft...
addpath('obob_ownft/');

obob_init_ft;

addpath('functions');

%% load all the data...
datapath = 'data/erps';
allfiles = dir(fullfile(datapath, '*.mat'));
allfiles = {allfiles.name};

mkdir(folder);

for file = allfiles
  [~, subject_id, ~] = fileparts(file{1});
  
  load(fullfile(datapath, file{1}));
  
  % select data....
  cfg = [];
  cfg.channel = channels;
  cfg.latency = timeframe;
  
  allfields = fieldnames(erps);
  
  for field = allfields'
    if match_str(data_selection, field{1})
      subfields = fieldnames(erps.(field{1}));
      for subfield = subfields'
        erps.(field{1}).(subfield{1}) = ft_selectdata(cfg, erps.(field{1}).(subfield{1}));
        data_to_csv(folder, subject_id, field{1}, subfield{1}, erps.(field{1}).(subfield{1}));
      end %for
    end %if
  end %for
end %for
